﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypta
{
    class Program
    {
        #region const
        const string _ahelp = "help";
        const string _aencode = "encrypt";
        const string _adecode = "decrypt";
        const string _ainput = "input";
        const string _akey = "key";
        const string _aapp = "app";
        const string _asecret = "secret";
        const string _alog = "log";
        const string _aview = "view";
        const string _productSplash = "data:ICAgX19fXyAgICAgICAgICAgICAgICAgIF8gICAgICAgIF8gICAgICAgDQogIC8gX19ffF8gX18gXyAgIF8gXyBfXyB8IHx" +
                                        "fIF9fIF8oXylfIF9fICANCiB8IHwgICB8ICdfX3wgfCB8IHwgJ18gXHwgX18vIF9gIHwgfCAnXyBcIA0KIHwgfF9fX3wgfCAgfCB" +
                                        "8X3wgfCB8XykgfCB8fCAoX3wgfCB8IHwgfCB8DQogIFxfX19ffF98ICAgXF9fLCB8IC5fXy8gXF9fXF9fLF98X3xffCB8X3wNCiA" +
                                        "gICAgICAgICAgICB8X19fL3xffCAgICAgICAgICAgICAgICAgICAgIA==";
        #endregion

        static void Main(string[] args)
        {
            using (MF.Mods.ConsoleX _conx = new MF.Mods.ConsoleX())
            {
                using (MF.Implementation.CryptaText _cryptatext = new MF.Implementation.CryptaText())
                {
                    // Initialize
                    _conx.Splash = MF.FX.StrResLoad(_productSplash).ToString();
                    _conx.SplashInformation = MF.FX.StrLocalization("Encryption module", "Модуль шифрования") + ": " + MF.Implementation.CryptaText.VersionModule + ", Default ext: " + MF.Implementation.CryptaText.DefaultCryptaExtFile;
                    _conx.ReadArguments(args, _ahelp + "," + _ahelp[0].ToString() + ";" + _aencode + "," + _aencode[0].ToString() + ";" + _adecode + "," + _adecode[0].ToString() + ";" +
                                              _ainput + "," + _ainput[0].ToString() + ";" + _aapp + "," + _aapp[0].ToString() + ";" + _aview + "," + _aview[0].ToString() + ";" +
                                              _akey + "," + _akey[0].ToString() + ";" + _asecret + "," + _asecret[0].ToString() + ";" + _alog + "," + _alog[0].ToString(), ":");
                    _conx.PrintAbout();
                    if (_conx.Arguments.ContainsKey(_ahelp) || _conx.Arguments.Count == 0)
                    {
                        // Print help
                        _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Header, _ahelp.ToUpper(), true);
                        _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, MF.FX.FileAppAssembly("AssemblyProductAttribute") + "\n\t[" + _ahelp + "]\n\t[" + _aencode + "|" + _adecode + "]\n\t[" + _ainput + ":path]\n\t[" + _akey + ":keyword|" + _aapp + ":path [" + _asecret + ":secretword]]\n\t[" + _alog + ":path]\n\t[" + _aview + "]", true);
                        _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, "ex1: " + MF.FX.FileAppAssembly("AssemblyProductAttribute") + " " + _adecode + " " + _ainput + ":c:\\f.cls " + _akey + ":123 " + _alog + ":\"c:\\x.log\" " + _aview, true);
                        _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, "ex2: " + MF.FX.FileAppAssembly("AssemblyProductAttribute") + " " + _aencode + " " + _ainput + ":c:\\f.opn " + _aapp + ":d:\\a.exe " + _asecret + ":s7", true);
                        _conx.PressAnyKey();
                    }
                    else
                    {
                        // Set log
                        _conx.LogPath = (_conx.Arguments.ContainsKey(_alog) ? _conx.Arguments[_alog] : "");
                        int _action = (_conx.Arguments.ContainsKey(_aencode) ? 1 : (_conx.Arguments.ContainsKey(_adecode) ? 2 : 0));
                        if (_action > 0)
                        {
                            // Action
                            _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Header, (_action == 1 ? _aencode : _adecode).ToUpper());
                            string _finput = (_conx.Arguments.ContainsKey(_ainput) ? _conx.Arguments[_ainput] : "");
                            if (_finput.Length > 0)
                            {
                                // Options for action
                                string _data = "";
                                string _key = (_conx.Arguments.ContainsKey(_akey) ? _conx.Arguments[_akey] : "");
                                string _app = (_conx.Arguments.ContainsKey(_aapp) ? _conx.Arguments[_aapp] : "");
                                string _secret = (_conx.Arguments.ContainsKey(_asecret) ? _conx.Arguments[_asecret] : "");
                                MF.Implementation.CryptaText.TokenType _tokentype = (_key.Length > 0 ? MF.Implementation.CryptaText.TokenType.Key : (_app.Length > 0 ? MF.Implementation.CryptaText.TokenType.Application : MF.Implementation.CryptaText.TokenType.Open));
                                try
                                {
                                    List<string> _finputlist = MF.FX.FileList(_finput);
                                    if (_finputlist.Count > 0)
                                    {
                                        _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, MF.FX.StrLocalization("Founded input files", "Найдено входных файлов") + ": " + _finputlist.Count.ToString(), true);
                                        foreach (string _fi in _finputlist)
                                        {
                                            _conx.Print(1);
                                            _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Point, _fi, true);
                                            // Input
                                            _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, MF.FX.StrLocalization("Read", "Чтение") + " " + _fi + "...");
                                            _data = System.IO.File.ReadAllText(_fi);
                                            _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.OK, MF.FX.StrSize(_data.Length));
                                            // Encrypt / Decrypt
                                            _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, (_action == 1 ? MF.FX.StrLocalization("Encrypt", "Шифрование") : MF.FX.StrLocalization("Decrypt", "Дешифрование")) + "...");
                                            if (_action == 1)
                                            { _data = _cryptatext.PillWrite(_tokentype, (_tokentype == MF.Implementation.CryptaText.TokenType.Key ? _key : _app), _secret, _data); }
                                            else
                                            { _data = _cryptatext.PillRead(_tokentype, (_tokentype == MF.Implementation.CryptaText.TokenType.Key ? _key : _app), _secret, _data); }
                                            _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.OK, MF.FX.StrSize(_data.Length));
                                            // Override Output for multifiles
                                            string _ftarget = _fi + (_action == 1 ? "." + MF.Implementation.CryptaText.DefaultCryptaExtFile : "~");
                                            // Output
                                            if (!_conx.Arguments.ContainsKey(_aview))
                                            {
                                                // Delete file
                                                if (System.IO.File.Exists(_ftarget))
                                                {
                                                    _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, MF.FX.StrLocalization("Delete", "Удаление") + " " + _ftarget + "...");
                                                    System.IO.File.Delete(_ftarget);
                                                    _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.OK, "");
                                                }
                                                // Output to file
                                                _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, MF.FX.StrLocalization("Write", "Запись") + " " + _ftarget + "...");
                                                System.IO.File.WriteAllText(_ftarget, _data);
                                            }
                                            else
                                            {
                                                // Output to display
                                                _conx.PrintSeparator(MF.FX.StrLocalization("Begin", "Начало"));
                                                _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Info, _data, true);
                                                _conx.PrintSeparator(MF.FX.StrLocalization("End of data", "Конец данных"));
                                                _conx.PressAnyKey();
                                            }
                                            _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.OK, "");
                                        }
                                    }
                                    else
                                    { _conx.Print(MF.Mods.ConsoleX.ConsoleMessageType.Warning, MF.FX.StrLocalization("Input files not found", "Входные файлы не найдены")); }
                                }
                                catch (Exception _ex)
                                { _conx.PrintError(_ex); }
                            }
                            else
                            { _conx.PrintError(MF.FX.StrLocalization("Do not specify a source file", "Не указан исходный файл")); }
                        }
                        else
                        { _conx.PrintError(MF.FX.StrLocalization("Undefined action", "Неопределенное действие")); }
                    }
                }
                _conx.PressAnyKey();
            }
        }
    }
}
